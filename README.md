Device Detect
=============
Drupal 8 module for detect devices and add cache context per device.


This is a lightweight mobile detection based on the [Mobile_Detect.php](http://mobiledetect.net/) library.

PHP examples
------------

### Check type device
``` php
$deviceDetector = \Drupal::service('detect.device');
$deviceDetector->isMobile();
$deviceDetector->isTablet()
```

Twig Helper Not fully working yet
-----------

```jinja
{% if is_mobile() %}
{% if is_tablet() %}
{% if is_device('iphone') %} 
{% if is_ios() %}
{% if is_android_os() %}
```
