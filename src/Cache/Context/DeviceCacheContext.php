<?php

namespace Drupal\detect_device\Cache\Context;

use Detection\MobileDetect;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\Cache\Context\RequestStackCacheContextBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the DeviceCacheContext service, for "per Device" caching.
 *
 * Cache context ID: 'device'.
 */
class DeviceCacheContext implements CacheContextInterface {

  /**
   * Mobile detect handler
   *
   * @var \Detection\MobileDetect
   */
  protected $deviceDetector;

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('Device');
  }

//  /**
//   * Constructs a new DeviceCacheContext service.
//   *
//   * @param \Detection\MobileDetect
//   *   The detection manager.
//   */
//  public function __construct(MobileDetect $deviceDetector) {
//    $this->deviceDetector = $deviceDetector;
//  }
//
//  public static function create(){
//    return new static(
//      \Drupal::service('detect.device')
//    );
//  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    $this->deviceDetector = \Drupal::service('detect.device');
    if ($this->deviceDetector->isMobile()){
      $device_type = 'mobile';
    } elseif ($this->deviceDetector->isTablet()) {
      $device_type = 'tablet';
    } else{
      $device_type = 'other';
    }
    return $device_type;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return new CacheableMetadata();
  }

}
