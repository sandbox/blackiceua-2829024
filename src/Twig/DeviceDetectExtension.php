<?php
namespace Drupal\detect_device\Twig;

use Detection\MobileDetect;
use Drupal\Core\Template\TwigExtension;
use Drupal\Core\Render\Renderer;
/**
 * DeviceDetectExtension class
 *
 */
class DeviceDetectExtension  extends TwigExtension
{
    /**
     * Mobile detect handler
     *
     * @var \Detection\MobileDetect
     */
    protected $deviceDetector;

    /**
     * Constructor
     *
     * @param Renderer $render
     * @param \Detection\MobileDetect $deviceDetector
     */
    public function __construct(Renderer $render, MobileDetect $deviceDetector)
    {
        parent::__construct($render);
        $this->deviceDetector = $deviceDetector;
    }
    
    /**
     * Get extension twig function
     * @return array
     */
    public function getFunctions()
    {
        //$aa = new \Twig_SimpleFunction::class;
        return array( 
            'is_mobile' => new \Twig_Function_Function(array($this, 'isMobile')),
            'is_tablet' => new \Twig_Function_Function(array($this, 'isTablet')),
            'is_device' => new \Twig_Function_Function(array($this, 'isDevice')),
            'is_ios' => new \Twig_Function_Function(array($this, 'isIOS')),
            'is_android_os' => new \Twig_Function_Function(array($this, 'isAndroidOS')),
            
        );
    }
    
    /**
     * Is mobile
     * @return boolean
     */
    public function isMobile()
    {
        return $this->deviceDetector->isMobile();
    }

    /**
     * Is tablet
     * @return boolean
     */
    public function isTablet()
    {
        return $this->deviceDetector->isTablet();
    }
    
    /**
     * Is device
     * @param string $deviceName is[iPhone|BlackBerry|HTC|Nexus|Dell|Motorola|Samsung|Sony|Asus|Palm|Vertu|...]
     *
     * @return boolean
     */
    public function isDevice($deviceName)
    {
        $magicMethodName = 'is' . strtolower((string) $deviceName);

        return $this->deviceDetector->$magicMethodName();
    }
    
    /**
     * Is iOS
     * @return boolean
     */
    public function isIOS()
    {
        return $this->deviceDetector->is('iOS');
    }

    /**
     * Is Android OS
     * @return boolean
     */
    public function isAndroidOS()
    {
        return $this->deviceDetector->is('Android');
    }
    
    /**
     * Extension name
     * @return string
     */
    public function getName()
    {
        return 'detect.device.twig.extension';
    }
}
