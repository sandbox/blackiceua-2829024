<?php

namespace Drupal\detect_device\Plugin\Condition;

use Detection\MobileDetect;
use Drupal\Console\Bootstrap\Drupal;
use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Detect device' condition to enable a condition based in module selected status.
 *
 * @Condition(
 *   id = "detect_device",
 *   label = @Translation("Detect device"),
 * )
 */
class DetectDevice extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Mobile detect handler
   *
   * @var \Detection\MobileDetect
   */
  protected $deviceDetector;

  /**
   * Creates a new ExampleCondition instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Detection\MobileDetect $deviceDetector
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MobileDetect $deviceDetector) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->deviceDetector = $deviceDetector;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('detect.device')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form['devices'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Display when the device is'),
      '#default_value' => $this->configuration['devices'],
      '#options' => [
        'mobile' => 'Mobile',
        'tablet' => 'Tablet',
        'other' => 'Other',
      ]
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['devices'] = array_filter($form_state->getValue('devices'));
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'devices' => []
      ] + parent::defaultConfiguration();
  }

  /**
   * Evaluates the condition and returns TRUE or FALSE accordingly.
   *
   * @return bool
   *   TRUE if the condition has been met, FALSE otherwise.
   */
  public function evaluate() {
    if (empty($this->configuration['devices']) && !$this->isNegated()) {
      return TRUE;
    }
    $device = [];
    if ($this->deviceDetector->isMobile()) {
      $device[] = 'mobile';
    }
    elseif ($this->deviceDetector->isTablet()) {
      $device[] = 'tablet';
    }
    else {
      $device[] = 'other';
    }
    return (bool) array_intersect($this->configuration['devices'], $device);
  }

  /**
   * Provides a human readable summary of the condition's configuration.
   */
  public function summary() {
//    $module = $this->getContextValue('module');
//    $modules = system_rebuild_module_data();

//    $status = ($modules[$module]->status) ? t('enabled') : t('disabled');
    return $this->t('Enabled');
//    return t('The module @module is @status.', [
//      '@module' => $module,
//      '@status' => $status
//    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $con = $this->getContexts();
    $contexts = parent::getCacheContexts();
    $contexts[] = 'device';
    return $contexts;
  }
}
