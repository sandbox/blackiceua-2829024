<?php

namespace Drupal\detect_device_views\Plugin\views\pager;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\pager\SqlBase;

/**
 * The plugin to handle full pager per device.
 *
 * @ViewsPager(
 *   id = "detect_device",
 *   title = @Translation("Paged output, full pager per device"),
 *   short_title = @Translation("Full per Device"),
 *   help = @Translation("Paged output, full Drupal style per device"),
 *   theme = "pager",
 *   register_theme = FALSE
 * )
 */
class DetectDevicePager extends SqlBase implements CacheableDependencyInterface {


  /**
   * @return int
   */
  public function getItemsPerPage() {
    $device = \Drupal::service('detect.device');
    if ($device->isMobile()) {
      $items = $this->options['items_per_page_mobile'];
    }
    elseif ($device->isTablet()) {
      $items = $this->options['items_per_page_tablet'];
    }
    else {
      $items = $this->options['items_per_page_other'];
    }
    return $items;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['items_per_page_mobile'] = array('default' => 6);
    $options['items_per_page_tablet'] = array('default' => 9);
    $options['items_per_page_other'] = array('default' => 18);


    // Use the same default quantity that core uses by default.
    $options['quantity_mobile'] = array('default' => 3);
    $options['quantity_tablet'] = array('default' => 6);
    $options['quantity_other'] = array('default' => 9);

    $options['tags']['contains']['first'] = array('default' => $this->t('« First'));
    $options['tags']['contains']['last'] = array('default' => $this->t('Last »'));

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    unset($form['items_per_page']);
    $form['items_per_page_mobile'] = array(
      '#title' => $this->t('Items per page (mobile)'),
      '#type' => 'number',
      '#description' => $this->t('Enter 0 for no limit.'),
      '#default_value' => $this->options['items_per_page_mobile'],
      '#weight' => -10
    );
    $form['items_per_page_tablet'] = array(
      '#title' => $this->t('Items per page (tablet)'),
      '#type' => 'number',
      '#description' => $this->t('Enter 0 for no limit.'),
      '#default_value' => $this->options['items_per_page_tablet'],
      '#weight' => -9
    );
    $form['items_per_page_other'] = array(
      '#title' => $this->t('Items per page (other)'),
      '#type' => 'number',
      '#description' => $this->t('Enter 0 for no limit.'),
      '#default_value' => $this->options['items_per_page_other'],
      '#weight' => -8
    );

    $form['quantity_mobile'] = array(
      '#type' => 'number',
      '#title' => $this->t('Number of pager links visible on mobile device'),
      '#description' => $this->t('Specify the number of links to pages to display in the pager on mobile device.'),
      '#default_value' => $this->options['quantity_mobile'],
    );

    $form['quantity_tablet'] = array(
      '#type' => 'number',
      '#title' => $this->t('Number of pager links visible on tablet device'),
      '#description' => $this->t('Specify the number of links to pages to display in the pager on tablet device.'),
      '#default_value' => $this->options['quantity_tablet'],
    );

    $form['quantity_other'] = array(
      '#type' => 'number',
      '#title' => $this->t('Number of pager links visible on other device'),
      '#description' => $this->t('Specify the number of links to pages to display in the pager on other device.'),
      '#default_value' => $this->options['quantity_other'],
    );

    $form['tags']['first'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('First page link text'),
      '#default_value' => $this->options['tags']['first'],
      '#weight' => -10,
    );

    $form['tags']['last'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Last page link text'),
      '#default_value' => $this->options['tags']['last'],
      '#weight' => 10,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function summaryTitle() {
    if (!empty($this->options['offset'])) {
      return $this->t('Paged: Mobile - @mobile, Tablet - @tablet, Other - @other. Offset - @offset', array(
        '@mobile' => $this->options['items_per_page_mobile'],
        '@tablet' => $this->options['items_per_page_tablet'],
        '@other' => $this->options['items_per_page_other'],
        '@offset' => $this->options['offset'],
      ));
    }
    return $this->t('Paged: Mobile - @mobile, Tablet - @tablet, Other - @other', array(
      '@mobile' => $this->options['items_per_page_mobile'],
      '@tablet' => $this->options['items_per_page_tablet'],
      '@other' => $this->options['items_per_page_other'],
    ));
  }

  public function getQuantity() {
    $device = \Drupal::service('detect.device');
    if ($device->isMobile()) {
      $quantity = $this->options['quantity_mobile'];
    }
    elseif ($device->isTablet()) {
      $quantity = $this->options['quantity_tablet'];
    }
    else {
      $quantity = $this->options['quantity_other'];
    }
    return $quantity;
  }

  /**
   * {@inheritdoc}
   */
  public function render($input) {
    // The 0, 1, 3, 4 indexes are correct. See the template_preprocess_pager()
    // documentation.
    $tags = array(
      0 => $this->options['tags']['first'],
      1 => $this->options['tags']['previous'],
      3 => $this->options['tags']['next'],
      4 => $this->options['tags']['last'],
    );
    return array(
      '#theme' => $this->themeFunctions(),
      '#tags' => $tags,
      '#element' => $this->options['id'],
      '#parameters' => $input,
      '#quantity' => $this->getQuantity(),
      '#route_name' => !empty($this->view->live_preview) ? '<current>' : '<none>',
    );
  }

  public function query() {
    if ($this->itemsPerPageExposed()) {
      $query = $this->view->getRequest()->query;
      $items_per_page = $query->get('items_per_page');
      if ($items_per_page > 0) {
        $this->options['items_per_page'] = $items_per_page;
      }
      elseif ($items_per_page == 'All' && $this->options['expose']['items_per_page_options_all']) {
        $this->options['items_per_page'] = 0;
      }
    }
    if ($this->isOffsetExposed()) {
      $query = $this->view->getRequest()->query;
      $offset = $query->get('offset');
      if (isset($offset) && $offset >= 0) {
        $this->options['offset'] = $offset;
      }
    }

    $limit = $this->getItemsPerPage();
    $offset = $this->current_page * $this->getItemsPerPage() + $this->options['offset'];
    if (!empty($this->options['total_pages'])) {
      if ($this->current_page >= $this->options['total_pages']) {
        $limit = $this->getItemsPerPage();
        $offset = $this->options['total_pages'] * $this->getItemsPerPage();
      }
    }

    $this->view->query->setLimit($limit);
    $this->view->query->setOffset($offset);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $contexts = parent::getCacheContexts();
    $contexts[] = 'device';
    return $contexts;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return [];
  }

}
